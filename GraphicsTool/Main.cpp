#include <iostream>
#include "MainGame.h"


int main(int argc, char** argv) {
	//Creating a variable that calls our MainGame in the header file
	MainGame mainGame;
	//Executing the run method created in the MainGame.cpp file
	mainGame.run();

	return 0;
}