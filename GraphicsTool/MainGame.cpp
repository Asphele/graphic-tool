#include "MainGame.h"
#include <iostream>;
#include <string>

//Creating a method so we can catch errors
void fatalError(std::string errorString) {
	std::cout << errorString << std::endl;
	std::cout << "Use any key to quit";
	int tmp;
	std::cin >> tmp;
	SDL_Quit();
}

//Creating our class and defining the variables that were declared in the header file
MainGame::MainGame() {
	_window = nullptr;
	//Assigning our client height and width can be changed to your preference
	_screenWidth = 1024;
	_screenHeight = 768;
	//Puts the gameState into Play from our Enum class we assigned in our header file
	_gameState = GameState::PLAY;
	
}


MainGame::~MainGame() {


}

//Boots the window up
void MainGame::run() {
	initSystems();

	gameLoop();
}

void MainGame::initSystems() {
	//Initialize SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	//Creating the window with the given arguments
	_window = SDL_CreateWindow("Game Engine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _screenWidth, _screenHeight, SDL_WINDOW_OPENGL);
	
	//Checking if there's any problem initializing the client, if so our error handler method will be executed
	if (_window == nullptr) {
		fatalError("SDL Window could not be created");
	}

	SDL_GLContext glContext = SDL_GL_CreateContext(_window);

	if (glContext == nullptr) {
		fatalError("SDL_GL context could not be created!");
	}

	GLenum error = glewInit();
	if (error != GLEW_OK) {
		fatalError("Could not initialize glew!");
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	glClearColor(0.0f ,0.0f ,1.0 ,1.0);
}

//Keeps our window running without closing until being closed itself
void MainGame::gameLoop() {
	while (_gameState != GameState::EXIT) {
		processInput();
		drawGame();
	}
}
void MainGame::processInput() {
	//Creating event variable so we can use all the event properties from SDL
	SDL_Event evnt;

	//Checking if there is any events
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type)
		{
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			std::cout << evnt.motion.x << " " << evnt.motion.y << std::endl;
			break;

		case SDL_MOUSEBUTTONDOWN:
			std::cout << "You clicked" << std::endl;
			break;
		}
	}
}

void MainGame::drawGame() {
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnableClientState(GL_COLOR_ARRAY);
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0, 0);
	glVertex2f(0, 500);
	glVertex2f(500, 500);
	glEnd();

	SDL_GL_SwapWindow(_window);
}