#pragma once


#include <SDL/SDL.h>
#include <GL/glew.h>

//Creating a Enum to we can keep our window running or close it down
enum class GameState {
	PLAY, EXIT
};

class MainGame
{
public:
	//Making variables and methods so it can be assigned in the cpp file

	MainGame();
	~MainGame();
	void run();

private:
	void initSystems();
	void gameLoop();
	void processInput();
	void drawGame();

	SDL_Window* _window;
	int _screenWidth;
	int _screenHeight;
	GameState _gameState;
};

